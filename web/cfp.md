---
layout: page
title: "Call for Participation"
permalink: /cfp/
---

# The Call for Talks period is now closed

Speaker announcements will begin soon. 

In the meantime, please remember to 
register to attend LAS. 

 <button class="button" onclick="https://linuxappsummit.org/register'">Registration Page</button>


*Note: If you submitted a talk, you can access your speaker profile 
information, and edit anything you need to, through the registration portal*

------------------

<div class="las-banner" style="background-color: #982c7e; color: white;" markdown="1">

## Talk Tracks to Expect at LAS
* Creating, packaging, and distributing applications
* Design and usability
* Commercialization
* Community
* Platform
* Linux App Ecosystem

## Important Dates for Speakers
 * **31 July** - CfP opens. Submit your talk idea!
 * **Week of 8 Sept** - CfP closes.
 * **Week of 23 Sept** - Speaker notification begins.

</div>

