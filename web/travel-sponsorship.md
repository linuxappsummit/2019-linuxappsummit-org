---
layout: page
title: "Travel Sponsorship"
permalink: /travel-sponsorship
---

## Travel Sponsorship Requests
If you need help getting to and staying in Barcelona, we’d appreciate it if you 
could ask your organization or company to sponsor the trip. If that’s not 
possible, we’ll do our best to sponsor some participants.

We have limited funds available, so submititng an application is not a 
guarantee of receiving sponsorship. 

**Please make travel sponsorship requests by Friday, September 27th.** 


### GNOME Foundation Members
Please apply for travel sponsorship via GNOME's travel 
committee: [https://wiki.gnome.org/Travel](https://wiki.gnome.org/Travel)

### KDE Contributors
Please apply for travel sponsorship via KDE e.V.'s travel 
committee: [https://reimbursements.kde.org/events/107](https://reimbursements.kde.org/events/107)

### All Others

Some communities, like 
[Ubuntu](https://discourse.ubuntu.com/t/linux-application-summit-barcelona/12477), 
have funds that community members can 
request to attend LAS. Please check to see if there is anything that covers you. 

Otherwise, please send us a request by e-mailing 
[travel@linuxappsummit.org](travel@linuxappsummit.org) with:

 * Your name
 * The amount you will need and a breakdown of costs
 * If you are a GNOME or KDE Foundation member (or if you are not)
 * What you are planning to bring to the conference so we can consider your application. 
For example, are you submitting a talk, are you hoping to volunteer, etc? 
These are the types of things that help increase your chances of receiving 
travel sponsorship.

After we receive your request, we may ask you for additional information or may 
ask that you perform additional steps to finish processing your request.
Please note that we will need receipts from all payments to have them reimbursed. 

---
**If you will require a visa to attend LAS, please 
request a [visa invitation letter](https://linuxappsummit.org/visa/) 
as soon as possible.**


