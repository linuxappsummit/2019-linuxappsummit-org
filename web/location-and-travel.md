---
layout: page
title: "Location + Travel"
permalink: /location/
---

# Venue
The conference will take place at 
[Lleialtat Santsenca](https://www.openstreetmap.org/way/264735742),
a chic community center located in the Sants neighborhood of Barcelona, Spain. 
It hosts many meetups and conferences, and has been featured in 
[architectural articles](https://www.archdaily.com/889515/civic-centre-lleialtat-santsenca-1214-harquitectes) 
due to the startling contrast between its art deco facade and 
its minimalist interior. 

Lleialtat Santsenca is conveniently located close to the 
main central station of Sants Estació in Barcelona.

This venue has 3 floors and is completely accessible through elevators. 
The spaces include a main hall, and a few smaller meeting spaces. 
While there is WiFi connectivity, there is also wired connectivity 
in most rooms. 


---------------------------


# Acommodation
There are many accommodation options close to the venue because of its 
convenient and central location. 

Here are a few suggestions for where to stay. 

### Smart Room Barcelona
~56€/night (Breakfast included)  
2 min walk to venue  

<a href="http://www.smartroombarcelona.com">Website</a>  


### Live and Dream Hostel Barcelona
~75€/night (Breakfast not included)  
3 min walk to venue  

<a href="https://live-dream.hotelbcn-barcelona.com/">Website</a>  


### Hostal Sans Barcelona
~55€/night (Breakfast not included) or ~65€/night (Breakfast included)  
5 min walk to venue 

<a href="http://www.hostalsans.com/">Website</a>


-----------------

# Travel and Transportation

### Arriving by Plane
Barcelona's international airport, 
[Barcelona–El Prat Josep Tarradellas Airport (BCN)](https://en.wikipedia.org/wiki/Barcelona%E2%80%93El_Prat_Josep_Tarradellas_Airport),
also known as El Prat Airport, is the most convenient place to fly into. 
It has plenty of connections with Europe, America, and Asia.

You can take a train from the airport to 
Sants Estació station, which is a 10 minute walk to the conference venue. 


### Arriving by Train

**The conference venue is very close to the main central station: Sants Estació (10
minutes by foot).** 

Barcelona has several High Speed Trains which travel to and from 
Madrid, Paris, and other major European cities.

All trains, local, regional, and international, call at Sants Estació station, 
including the line that connects with the airport. Sants Estació station also 
has connections with 2 metro lines, and lots of local and regional buses.

### Arriving by Bus or Metro

**The closest metro station to the venue is Plaça de Sants**.

The Barcelona Metro and Bus system is quite good and one should be able to get
anywhere in the city with it. 

### Taxi Services

There is an app called "FREE NOW" (formerly "mytaxi") available in the [Google Play Store](https://play.google.com/store/apps/details?id=taxi.android.client&hl=en_US) and [Apple Store](https://apps.apple.com/us/app/free-now-mytaxi/id357852748). This app can be used to book taxis in Barcelona.
