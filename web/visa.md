---
layout: page
title: "Visas and Invitation Letters"
permalink: /visa
---
## Visa Invitation Letters
If you require a visa to attend LAS, you can request an invitation letter by 
emailing: [aleixpol@kde.org](aleixpol@kde.org). 

Please cut and paste the following into the e-mail and provide all the answers:

 * Name and address of the embassy or consulate general where you will apply for the visa
 * Full name, as it appears on the passport:
 * Last name:
 * Pronoun (he/she/they):
 * Living place address:
 * Name and address of workplace:
 * Dates of stay in the city where the event will take place (check in date - check out date):
 * Name and address of the place of stay:
 * Date of birth:
 * Passport number:
 * Reasons to be at the event, your motivation, the title of your talk (if relevant)
 * Links that show your involvement in relevant activites or work (blog, commits, mailing list archives, and/or Bugzilla history):
 

If you have approved sponsorship by LAS, KDE or GNOME, 
please include the following information, which will be 
cross-checked with the travel committee:

 * Sponsoring organization (KDE, GNOME, LAS)
 * Amount sponsored for air travel:
 * Number of nights sponsored for accommodations:
 * Dates for the nights sponsored (check in date - check out date):
 * Is the sponsorship full (does it fully cover the cost of the inter-city travel and the nights you need to stay for the event):
 

