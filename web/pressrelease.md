---
layout: page
title: "Press"
permalink: /pressrelease
---

The GNOME Foundation and KDE e.V. are proud to announce the first year of jointly organizing the Linux App Summit (LAS). LAS will be held in Barcelona from November 12th to 15th, 2019 and is the first collaborative event co-hosted by the two organizations since the Desktop Summit in 2009. Both organizations are eager to bring their communities together in building an application ecosystem that transcends individual distros and broadens the market for everyone involved.

No longer taking a passive role in the free desktop sector, with the joint influence of the two desktop projects - LAS will shepherd the growth of the FOSS desktop by building an ecosystem that will encourage the creation of quality applications, seek opportunities for compensation for FOSS developers, and foster a thriving market for the Linux operating system.

GNOME's executive director, Neil McGovern says, _"LAS represents one of many steps towards a thriving desktop eco-system. By partnering with KDE we show the desire  to build the kind of application eco-system that demonstrates that open source and free software is important; the technology and organization we build to achieve this is valuable and necessary."_, LAS will be the intersection of application developers, designers, user and kernel space engineers to work together in building an ecosystem that could create a new market for applications on Linux.

_"Over the years we have been creating great solutions that our users are using around the world. It's been when we have worked together, that we have managed to become bigger than the sum of the parts. Together with GNOME, counting with the collaboration of many distributions and application developers, we'll have the opportunity to work together, share our perspectives and offer the platform that the next generation of solutions will be built on."_, Aleix Pol Gonzalez, KDE e.V Vice-President says about the inaugural effort about LAS. As the first conference of its kind, we are interested in topics  that are centered around growing the application eco-system for Linux as well as providing a platform for others to share ideas and technology. With that in mind, the topics we are interested in are:

* Creating, packaging, and distributing applications
* Design and usability
* Commercialization
* Community / Legal
* Platform
* Linux App Ecosystem

The CfP starts today and ends on August 31st.  You may submit your talk ideas at [https://linuxappsummit.org/cfp/](https://linuxappsummit.org/cfp/).


_"I am excited to see GNOME and KDE working together on LAS, and I believe that the event will help put down strong foundations for collaborative cross-project development that would benefit Linux users across all distributions and on any compatible device. I hope to see widespread community support for the event and, as a user, I look forward to reaping the benefits of the seeds that have now been sown."_ - Christel Dahlskjaer, Private Internet Access and freenode Project Lead.

We look forward to seeing all of you in Barcelona and buliding the app eco-system together! For more information about LAS, please visit [https://linuxappsummit.org/](https://linuxappsummit.org/).

For more information on the event, send an e-mail to [info@linuxappsummit.org](mailto:info@linuxappsummit.org).

### About KDE e.V
The KDE® Community is a free software community dedicated to creating an open and user-friendly computing experience, offering an advanced graphical desktop, a wide variety of applications for communication, work, education and entertainment and a platform of libraries and frameworks that helps developers easily build new applications. We have a strong focus on finding innovative solutions to old and new problems, creating a dynamic atmosphere open for experimentation.

[https://ev.kde.org](https://ev.kde.org)

### About GNOME Foundation
The GNOME Foundation is an organization committed to supporting the advancement of GNOME, comprised of hundreds of volunteer developers and industry-leading companies. The Foundation is a member directed, 501(c)(3) non-profit organization that provides financial, organizational, and legal support to the GNOME project. The GNOME Foundation is supporting the pursuit of software freedom through the innovative, accessible, and beautiful user experience created by GNOME contributors around the world. More information about GNOME and the GNOME Foundation can be found at www.gnome.org and foundation.gnome.org. Become a friend of GNOME at [https://www.gnome.org/friends/](https://www.gnome.org/friends/).

[https://www.gnome.org/foundation](https://www.gnome.org/foundation)
